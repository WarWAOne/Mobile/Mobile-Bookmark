package nothing.bookmarks

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Created by xn1a on 24/07/17.
 */

@Entity
class CategoryTag {
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null

    val category_id: Int? = null
    val tag_id: Int? = null
}