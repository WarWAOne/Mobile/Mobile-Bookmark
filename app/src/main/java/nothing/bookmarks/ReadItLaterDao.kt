package nothing.bookmarks

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by xn1a on 19/07/17.
 */

@Dao
interface ReadItLaterDao {
    @get:Query("SELECT * FROM ReadItLater")
    val all: List<ReadItLater>

    @Insert
    fun insertAll(vararg readItLaters: ReadItLater)

    @Delete
    fun delete(readItLater: ReadItLater)
}