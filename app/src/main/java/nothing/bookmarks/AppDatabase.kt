package nothing.bookmarks

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database

/**
 * Created by xn1a on 19/07/17.
 */

@Database(entities = arrayOf(Category::class, Tag::class, ReadItLater::class, Bookmark::class,
        BookmarkTag::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun bookmarkDao(): BookmarkDao
    abstract fun tagDao() : TagDao
    abstract fun readItLater(): ReadItLaterDao
}
