package nothing.bookmarks

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by xn1a on 19/07/17.
 */

@Dao
interface BookmarkDao {
    @get:Query("SELECT * FROM Bookmark")
    val all: List<Bookmark>

    @Insert
    fun insertAll(vararg bookmarks: Bookmark)

    @Delete
    fun delete(bookmark: Bookmark)
}