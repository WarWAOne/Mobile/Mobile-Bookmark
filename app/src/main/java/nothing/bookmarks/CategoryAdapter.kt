package nothing.bookmarks

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by xn1a on 19/07/17.
 */

class CategoryAdapter(list: List<Category>) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    //private val mContext: Context = null!!
    var mList: List<Category> = null!!

    init {
        mList = list
    }

    override fun getItemCount(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: CategoryViewHolder?, position: Int) {
        val category: Category = mList[position]
        holder?.tv_name?.text = category.name
        //holder.imv_ico.src = getResourceDrawable(category.icon)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoryViewHolder {
        var view: View = LayoutInflater.from(parent?.context).inflate(R.layout.item_category, parent, false)
        var viewHolder = CategoryViewHolder(view)
        return viewHolder
    }

    class CategoryViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var tv_name: TextView? = itemView?.findViewById<TextView>(R.id.name)
        //var imv_icon: ImageView? = null
        //imv_icon = itemView.findViewById(R.id.icon)

    }

}