package nothing.bookmarks

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by xn1a on 19/07/17.
 */

@Entity
class BookmarkTag {
    @PrimaryKey(autoGenerate = true)
    val id = null

    val bookmark_id = null

    val tag_id = null
}