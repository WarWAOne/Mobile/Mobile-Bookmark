package nothing.bookmarks

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query


/**
 * Created by xn1a on 19/07/17.
 */

@Dao
interface CategoryDao {
    @get:Query("SELECT * FROM Category")
    val all: List<Category>

    @Insert
    fun insertAll(vararg categories: Category)

    @Delete
    fun delete(category: Category)
}
