package nothing.bookmarks

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

/**
 * Created by xn1a on 19/07/17.
 */

@Entity(foreignKeys = arrayOf(ForeignKey(entity = Category::class,
        parentColumns = arrayOf("name"),
        childColumns = arrayOf("category"))))

open class Bookmark {
    @PrimaryKey(autoGenerate = true)
    private val id = null

    private val name: String? = null

    private val url: String? = null

    private val favicon: String? = null

    private val category: String? = null
}