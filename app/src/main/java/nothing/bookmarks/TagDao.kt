package nothing.bookmarks

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by xn1a on 19/07/17.
 */

@Dao
interface TagDao {
    @get:Query("SELECT * FROM Tag")
    val all: List<Tag>

    @Insert
    fun insertAll(vararg tags: Tag)

    @Delete
    fun delete(tag: Tag)
}