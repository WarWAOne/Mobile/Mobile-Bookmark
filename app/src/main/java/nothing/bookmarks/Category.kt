package nothing.bookmarks

import android.arch.lifecycle.ViewModel
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by xn1a on 19/07/17.
 */

@Entity
class Category {
    @PrimaryKey
    val name: String? = null

    val iconUrl: String? = null

    // Getters and setters are ignored for brevity,
    // but they're required for Room to work.
}
